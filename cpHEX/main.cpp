#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>




int main(int argc, char* argv[])
{
	FILE* in_file = nullptr;
	FILE* out_file = nullptr;
	char in[255] = "";
	char out[255] = "";
	char symb[255] = "";

	char buf;


	if (argc < 5)
	{
		printf("two required parameters -i inputFile -o outputFile\n");
	}
	if (argc > 7)
	{
		printf("so mach more arguments\n");
		return 1;
	}

	for (int i = 1; i < argc; ++i)
	{
	


		if (!(strcmp(argv[i], "-h")))
		{
			printf("-i - input file\n");
			printf("-o - output file\n");
			printf("-s - symbol\n");
			printf("-h - help\n");
			return 2;
		}
		if (!(strcmp(argv[i], "-i")))
		{
			if (argv[i + 1] == 0x00)
			{
				printf("need name for input file\n");
				return 3;
			}
			strcpy_s(in,255,argv[i + 1]);
		}

		if (!(strcmp(argv[i], "-o")))
		{
			if (argv[i + 1] == 0x00)
			{
				printf("need name for output file\n");
				return 4;
			}
			strcpy_s(out,255, argv[i + 1]);
			
		}
		if (!(strcmp(argv[i], "-s")))
		{

			if (argv[i + 1] == 0x00)
			{
				printf("need symbol\n");
				return 4;
			}
			strcpy_s(symb, 255, argv[i + 1]);
		}
		
	}


	int errorNumberInfile = fopen_s(&in_file, in, "rb");
	if (errorNumberInfile) 
	{
		printf("errorNumberInfile %d\n", errorNumberInfile);
		return 5;

	}

	int errorNumberOutfile = fopen_s(&out_file, out, "wb");
	if (errorNumberOutfile)
	{
		printf("errorNumberOutfile %d\n", errorNumberOutfile);
		return 6;

	}

	fseek(in_file, 0, SEEK_END);
	unsigned int fileSize = ftell(in_file);
	fseek(in_file, 0, SEEK_SET);


	if(symb==0x00)
		for (unsigned int i = 0; i < fileSize; ++i)
		{
			fscanf_s(in_file, "%c", &buf, sizeof(buf));
			fprintf_s(out_file, "%.2hhX", buf);

		}
	else
	{
		for (unsigned int i = 0; i < fileSize; ++i)
		{
			fprintf_s(out_file, "%s", symb);
			fscanf_s(in_file, "%c", &buf, sizeof(buf));
			fprintf_s(out_file, "%.2hhX", buf);

		}
	}

	fclose(in_file);

	fclose(out_file);

	return 0;
}